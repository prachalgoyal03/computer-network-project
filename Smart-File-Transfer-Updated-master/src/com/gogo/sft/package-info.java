/**
 *         Classes Information :-
 * 
 *         1. Client : Client class contains entire GUI + Logic for Client (file
 *         reciever).
 * 
 *         2. Menu : The class which allows you to choose between client and
 *         server (or what you want to do).This is the main class
 * 
 *         3. MyJButton : A class extending JButton (javax.swing.JButton) and
 *         overriding certain GUI properties for my own ease.
 * 
 *         4. MyJLabel :A class extending JLabel (javax.swing.JLabel) and
 *         overriding certain GUI properties for my own ease.
 * 
 *         5. Server : Server class contains entire GUI + Logic for Server (file
 *         sender).
 * 
 */
package com.gogo.sft;